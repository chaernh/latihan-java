package polymorphism.notifikasi;

public class SMSNotification implements interfaceNotifikasi {
	@Override
    public void sendMsg(String receiver, String content) {
        System.out.println("Mengirim SMS ke " + receiver + " dengan isi:");
        System.out.println(content);
    }
}
