package polymorphism.notifikasi;

public class PushNotification implements interfaceNotifikasi {
	@Override
    public void sendMsg(String receiver, String content) {
        System.out.println("Mengirim Push Notif ke " + receiver + " dengan isi:");
        System.out.println(content);
    }
}
