package polymorphism.notifikasi;

public class EmailNotification implements interfaceNotifikasi {
	@Override
    public void sendMsg(String receiver, String content) {
        System.out.println("Mengirim email ke " + receiver + " dengan isi:");
        System.out.println(content);
    }
}
